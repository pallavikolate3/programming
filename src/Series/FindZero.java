package Series;

public class FindZero {
    public static void main(String[] args) {
        long a=1110001021301l;
        int count=0;
        while(a!=0){
            int r= (int) (a%10);
            if(r==0){
                count++;
            }
            a=a/10;
        }
        System.out.println(count);
    }
}
