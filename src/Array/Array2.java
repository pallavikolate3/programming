package Array;

public class Array2 {
    public static void main(String[] args) {
        int[] arr1={2,4,6,8,10,12,14,16,18,20};
        int sum=0;
        for(int a:arr1){
            sum+=a;
        }
        System.out.println("Sum:"+sum);
        double avg=sum/arr1.length;
        System.out.println("Average:"+avg);
    }
}
