package Array;

public class Array1 {
    public static void main(String[] args) {
        int[] arr={1,2,3,4};
        //Enhanced for loop syntax=for(datatype storeVariable : arrayName)
        for (int a:arr)
        {
            System.out.println(a);
        }
    }
}
