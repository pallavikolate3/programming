package sorting;

import Array.RandomArray;

import java.util.Random;

public class QuickSort {
    public static void main(String[] args) {
        int[] arr = RandomArray.randomArr(10);
        for (int a : arr)
            System.out.print(a + " ");
        System.out.println("=======");
        sort(arr, 0, arr.length - 1);

        for (int a : arr)
            System.out.print(a + "  ");
    }
    public static void sort(int[] arr,int l,int h) {
        if (l < h) {
            int idx = quickSort(arr, l, h);
            sort(arr, l, idx - 1);
            sort(arr, idx + 1, h);
        }
    }
    public static int quickSort(int[] arr,int l,int h)
        {
            int pivot = arr[h];
            int i = l - 1;

            for (int j = l; j < h; j++) {
                if (arr[j] <=pivot) {
                    i++;
                    int temp = arr[i];
                    arr[i] = arr[j];
                    arr[j] = temp;
                }
            }
            int temp = arr[i + 1];
            arr[i + 1] = arr[h];
            arr[h] = temp;

            return i+1;
        }
    }




