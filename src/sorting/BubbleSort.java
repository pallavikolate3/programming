package sorting;

public class BubbleSort {
    public static void main(String[] args) {
        int[] arr={5,3,1,2,6};
        for (int i=0;i< arr.length;i++)
        {
            for (int j=0;j< arr.length-1;j++)  //arr.length-1 unexcepted iteratoin reduce //arr.length-i-1 also perform when j-1
            {
                if (arr[j]>arr[j+1])
                {
                    int temp=arr[j];
                    arr[j]=arr[j+1];
                    arr[j+1]=temp;
                }
            }
        }
        for (int a:arr)
        {
            System.out.print(" "+a);
        }
    }
}
