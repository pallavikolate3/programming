package sorting;

public class Sort2 {
    public static void main(String[] args) {
        int[] arr={9,5,8,4,2,7};
        for (int i=0;i< arr.length;i++){
            int minIdx=i;
            for (int j=i+1;j<arr.length;j++) {
                if (arr[minIdx] > arr[j]) {
                    minIdx = j;
                }
            }
                int temp=arr[i];
                arr[i]=arr[minIdx];
                arr[minIdx]=temp;

        }
        for (int a:arr){
            System.out.print(a+" ");
        }
    }
}
