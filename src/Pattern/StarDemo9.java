package Pattern;

public class StarDemo9 {
    public static void main(String[] args) {
        int line = 5;
        int star = 1;
        char ch='A';
        for (int i = 0; i < line; i++) {
            //  int ch=1;
            for (int j = 0; j < star; j++) {
                System.out.print(ch);
        //        ch++;
            }

            System.out.println();

            if (i < 4) {
                star++;
                ch++;
                if (ch > 'E') {
                    ch = 'A';
                }
            } else {
                star--;
            }

        }
        }
}
