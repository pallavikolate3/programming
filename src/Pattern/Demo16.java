package Pattern;

public class Demo16 {
    public static void main(String[] args) {
        int line=5;
        int star=5;

        for (int i=0;i<line;i++) {
            int ch1 = 0;
            int ch2 = 1;

            for (int j = 0; j < star; j++) {
                if (j% 2 == 0) {
                    System.out.print(ch1);
                } else {
                    System.out.print(ch2);
                }
            }
        }       System.out.println();
    }
}
