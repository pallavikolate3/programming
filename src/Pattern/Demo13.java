package Pattern;

public class Demo13 {
    public static void main(String[] args) {
        int line = 5;
        int star = 5;
        char a = 'A';
        for (int i = 0; i < line; i++) {
            char b = a;

            for (int j = 0; j < star; j++) {
                System.out.print(b +"\t");
                b += 1;
            }
            System.out.println();
            a++;
        }
    }
}