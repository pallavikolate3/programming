package Pattern;

public class StarDemo3 {
    public static void main(String[] args) {
        int line = 4;
        int star = 7;
        char ch = 'A';
        for (int i = 0; i < line; i++) {
            for (int j = 0; j < star; j++) {
                System.out.print(ch + "\t");
            }
            System.out.println();
            ch++;
            star -= 2;

        }
    }   }


