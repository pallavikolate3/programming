package Pattern;

public class starDemo12 {
    public static void main(String[] args) {
        int line=3;
        int star=1;

        for (int i=0;i<line;i++){
            for (int j=0;j<star;j++)
            {
                System.out.print(" _* ");

            }
            System.out.println();
            star++;
        }
    }
}
