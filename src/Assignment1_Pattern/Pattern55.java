package Assignment1_Pattern;

public class Pattern55 {
    public static void main(String[] args) {
        int line = 9;

        int star = 1;

        for (int i = 0; i < line; i++) {
            int ch = 1;

            for (int j = 0; j < star; j++) {
                System.out.print(ch++ + " ");
            }
            System.out.println();
            if (i < 4) {
                ch++;
                star++;
            } else {
                ch--;
                star--;
            }
        }
    }
}

