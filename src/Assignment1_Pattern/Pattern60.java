package Assignment1_Pattern;

public class Pattern60 {
    public static void main(String[] args) {
        int line = 9;
        int space=0;
        int star = 5;
        int ch=1;
        for (int i = 0; i < line; i++) {
            int ch1 = ch;
            for (int k=0;k<space;k++){
                System.out.print(" ");
            }
            for (int j = 0; j < star; j++) {
                System.out.print(ch1++ + " ");
            }
            System.out.println();
            if (i < 4) {
                ch+=1;
                star--;
                space++;
            } else {
                ch-=1;
                star++;
                space--;
            }
        }
    }
}
