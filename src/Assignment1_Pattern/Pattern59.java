package Assignment1_Pattern;

public class Pattern59 {
    public static void main(String[] args) {
        int line = 9;
        int star = 5;
        int space=0;

        for (int i = 0; i < line; i++) {
            int ch = 1;
            for (int k = 0; k < space; k++){
                System.out.print(" ");
             }
            for (int j = 0; j < star; j++) {
                System.out.print(ch++);
            }
            System.out.println();
            if (i < 4) {
                ch++;
                star--;
                space++;
            } else {
                ch--;
                star++;
                space--;
            }
        }
    }
}
