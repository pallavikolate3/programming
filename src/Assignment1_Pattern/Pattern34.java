package Assignment1_Pattern;

public class Pattern34 {
    public static void main(String[] args) {
        int line=6;
        int star=6;

        for(int i=0;i<line;i++) {
            char ch = 'A';
            for (int j = 0; j < star; j++) {
                System.out.print(ch++);
            }
            System.out.println();
            star--;
        }
    }
}
