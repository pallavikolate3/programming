package Assignment1_Pattern;

public class Pattern20 {
    public static void main(String[] args) {
        int line=9;
        int space=0;
        int star=10;

        for (int i=0;i<line;i++){
            for (int j=0;j<star;j++){
                System.out.print("*");
            }
            for (int k=0;k<space;k++){
                System.out.print(" ");
            }
        }

    }
}
