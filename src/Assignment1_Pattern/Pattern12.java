package Assignment1_Pattern;

public class Pattern12 {
    public static void main(String[] args) {
        int line = 5;
        int space = 0;
        int star = 9;

        for (int i = 0; i < line; i++) {
            for (int k = 0; k < space; k++) {
                System.out.print(" ");
            }
            for (int j = 0; j < star; j++) {
                System.out.print("*");
            }
            System.out.println();
            space++;
            star -= 2;
        }
    }
}
