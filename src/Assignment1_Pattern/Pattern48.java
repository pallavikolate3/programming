package Assignment1_Pattern;

public class Pattern48 {
    public static void main(String[] args) {
        int line = 9;
        int star = 1;
        int ch=9;
        for (int i = 0; i < line; i++) {

            for (int j = 0; j < star; j++) {
                System.out.print(ch);
            }
            System.out.println();
            ch--;
            star++;
        }
    }
}
