package Assignment1_Pattern;

public class Pattern50 {
    public static void main(String[] args) {
        int line = 7;
        int star = 10;

        for (int i = 0; i < line; i++) {
            int ch=0;
            for (int j = 0; j < star; j++) {
                System.out.print(ch);
                System.out.print(ch);
                System.out.print(ch++);
            }
            System.out.println();

        }
    }
}
