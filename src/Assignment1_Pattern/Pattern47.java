package Assignment1_Pattern;

public class Pattern47 {
    public static void main(String[] args) {
        int line = 9;
        int star = 1;

        for (int i = 0; i < line; i++) {
            int ch=9;
            for (int j = 0; j < star; j++) {
                System.out.print(ch--);
            }
            System.out.println();
          //  ch--;
            star++;

        }
    }
}
