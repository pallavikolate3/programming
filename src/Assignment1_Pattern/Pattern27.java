package Assignment1_Pattern;

public class Pattern27 {
    public static void main(String[] args) {
        int line=9;
        int star=1;
        char ch='A';
        for (int i=0;i<line;i++) {

            for (int j = 0; j < star; j++) {
                System.out.print(ch);
            }
            System.out.println();
            if (i < 4) {
                star++;
                ch++;
            } else {
                star--;
                ch--;
            }
        }
    }
}
