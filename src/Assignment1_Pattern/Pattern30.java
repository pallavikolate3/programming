package Assignment1_Pattern;

public class Pattern30 {
    public static void main(String[] args) {
        int line=12;
        int star=6;
       // char ch='F';
        for (int i=0;i<line;i++) {
           char ch = 'F';
            for (int j = 0; j < star; j++) {
                System.out.print(ch--);
            }
            System.out.println();
            if (i < 5){
                star--;
             //   ch--;
            }else {
                star++;
                ch++;
            }
        }
    }
}
